const EndPoints = require("../constants/endpoints");

module.exports = (path) => {
    let service = { name: "unknown", host: "unknown" };

    if (EndPoints.omniknight.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "omniknight";
        service.host = process.env.OMNIKNIGHT_HOST;
    } else if (EndPoints.abaddon.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "abaddon";
        service.host = process.env.ABADDON_HOST;
    } else if (EndPoints.bristleback.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "bristleback";
        service.host = process.env.BRISTLEBACK_HOST;
    } else if (EndPoints.canary.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "canary";
        service.host = process.env.CANARY_HOST;
    } else if (EndPoints.chip.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "chip";
        service.host = process.env.CHIP_HOST;
    } else if (EndPoints.eagle.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "eagle";
        service.host = process.env.EAGLE_HOST;
    } else if (EndPoints.harrier.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "harrier";
        service.host = process.env.HARRIER_HOST;
    } else if (EndPoints.huskar.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "huskar";
        service.host = process.env.HUSKAR_HOST;
    } else if (EndPoints.lich.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "lich";
        service.host = process.env.LICH_HOST;
    } else if (EndPoints.lifestealer.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "lifestealer";
        service.host = process.env.LIFESTEALER_HOST;
    } else if (EndPoints.owl.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "owl";
        service.host = process.env.OWL_HOST;
    } else if (EndPoints.phoenix.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "phoenix";
        service.host = process.env.PHOENIX_HOST;
    } else if (EndPoints.pigeon.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "pigeon";
        service.host = process.env.PIGEON_HOST;
    } else if (EndPoints.pudge.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "pudge";
        service.host = process.env.PUDGE_HOST;
    } else if (EndPoints.keycloak.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "keycloak";
        service.host = process.env.KC_SERVER_URL;
    } else if (EndPoints.kotl.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "kotl";
        service.host = process.env.KOTL_HOST;
    } else if (EndPoints.slardar.findIndex((ep) => path.includes(ep)) >= 0) {
        service.name = "slardar";
        service.host = process.env.SLARDAR_HOST;
    }

    return service;
}