class CommonHelper {
  static isHttpsProtocol() {
    return process.env.HYPERTEXT_TRANSFER_PROTOCOL === "https";
  }
}

module.exports = CommonHelper;
