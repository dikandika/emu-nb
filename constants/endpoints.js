module.exports = class EndPoints {
    
    static owl = [
        "/api/v1/login-info",
        "/api/v1/pwdParam",
        "/api/v1/setForceChangePassword",
    ];
    
    static chip = [
        "/api/v1.0",
        "/api/v1.0/card-profile",
        "/api/v1.0/product",
        "/api/v1.0/account-type-param",
    ];
    
    static pigeon = [
        "/pigeon",
        "/apiv1/vlinkMessage",
        "/apiv1/customerRegistration",
    ];
    
    static canary = [
        "/apiv1/globalLimit",
        "/apiv1/transactionLog",
        "/apiv1/transactionLog/audit",
        "/apiv1/deviceTrail",
        "/apiv1/sysparam",
        "/apiv1/auditLog",
        "/apiv1/backofficeTrail"
    ];
    
    static eagle = [
        "/api/v1.0/master",
    ];
    
    static huskar = [
        "/huskar",
        "/huskar/image",
        "/huskar/upload",
    ];
    
    static lich = [
        "/lich",
    ]; 
    
    static omniknight = [
        "/omniknight",
        "/omniknight/lovs",
        "/omniknight/lovs/categories",
        "/omniknight/dukcapil-param",
    ]; 
    
    static phoenix = [
        "/phoenix",
        "/phoenix/customer",
        "/phoenix/log/customer",
        "/phoenix/history/customer",
    ]; 
    
    static harrier = [
        "/harrier",
        "/harrier/booking-quota",
        "/harrier/timetabled-calls"
    ]; 
    
    static abaddon = [
        "/abaddon",
        "/abaddon/profession",
        "/abaddon/industry",
        "/abaddon/branch-office",
        "/abaddon/number-of-transactions-risk-question",
        "/abaddon/number-of-transactions-risk-score",
        "/abaddon/identity-risk",
        "/abaddon/profile-risk",
        "/abaddon/business-location-risk",
        "/abaddon/business-activity-risk",
        "/abaddon/ownership-structure-risk",
        "/abaddon/number-of-transactions-risk",
        "/abaddon/products-and-services-risk",
        "/abaddon/other-information-risk"
    ]; 
    
    static lifestealer = [
        "/lifestealer",
        "/lifestealer/special-person",
        "/lifestealer/special-person-mapping",
    ]; 
    
    static pudge = [
        "/pudge",
        "/pudge/profile",
    ]; 
    
    static bristleback = [
        "/bristleback",
    ]; 

    static keycloak = [
        "/auth",
        "/auth/realms/visiondg/protocol/openid-connect/token",
        "/auth/realms/visiondg/protocol/openid-connect/logout",
        "/auth/admin/realms/visiondg/users",
        "/auth/admin/realms/visiondg/roles",
        "/auth/admin/realms/visiondg/roles-by-id",
        "/auth/admin/realms/visiondg/groups",
        "/auth/admin/realms/visiondg/clients/084674c0-a209-4b17-9f8f-5807f4b6004a/user-sessions",
        "/members",
        "/groups",
        "/role-mappings",
        "/role-mappings/realm",
        "/role-mappings/realm/available",
    ];

    static kotl = [
        "/kotl"
    ];

    static slardar = [
        "/slardar"
    ];

}