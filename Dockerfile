FROM node:lts-alpine

ENV TZ=Asia/Jakarta
RUN apk --update add tzdata
RUN cp /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /usr/src/app

RUN mkdir -p /aghanim/log/emu/
RUN mkdir -p /aghanim/config/emu/

COPY .env /aghanim/config/emu/

COPY package*.json ./

RUN yarn

COPY . .

EXPOSE 3030

CMD ["yarn", "start"]