const axios = require("axios");

function hasAuthorizationToken(headers) {
  if (
    headers.hasOwnProperty("authorization") &&
    headers["authorization"].includes("Bearer ")
  ) {
    return true;
  } else {
    return false;
  }
}

async function validateToken(bearer) {
  const response = await axios
    .get(
      `${process.env.KC_SERVER_URL}/auth/realms/${process.env.KC_REALM}/protocol/openid-connect/userinfo`,
      { headers: { Authorization: bearer } }
    )
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      return err.response;
    });

    return {status: response["status"], data: response["data"]};
}

const tokenValidation = async function (req, res, next) {
  if (!hasAuthorizationToken(req.headers)) {
    res.send("Sorry, your provided token information not exists");
  }

  const validationResult = await validateToken(req.headers["authorization"]);

  if (validationResult.status === 200) {
    next();
  } else {
    res.send(validationResult);
  }
};

module.exports = tokenValidation;
