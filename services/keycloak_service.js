const axios = require("axios");
const KeycloakHelper = require("../helper/keycloak");

class KeycloakService {
    constructor(baseUrl) {
        this.baseUrl = baseUrl;
    }

    async get(req) {

        const kcAdmin = await KeycloakHelper.getAdminToken();

        return await axios
            .get(`${this.baseUrl}${req.url}`, {
                headers: {
                    "Authorization": `Bearer ${kcAdmin.access_token}`
                }
            })
            .then(function (res) {
                return res;
            })
            .catch(function (err) {
                return err.response;
            });

    }

    async post(req) {
        const kcAdmin = await KeycloakHelper.getAdminToken();

        return await axios
            .post(`${this.baseUrl}${req.url}`, req.body, {
                headers: {
                    "Authorization": `Bearer ${kcAdmin.access_token}`
                }
            })
            .then(function (res) {
                return res;
            })
            .catch(function (err) {
                return err.response;
            });

    }

    async put(req) {
        const kcAdmin = await KeycloakHelper.getAdminToken();

        return await axios
            .put(`${this.baseUrl}${req.url}`, req.body, {
                headers: {
                    "Authorization": `Bearer ${kcAdmin.access_token}`
                }
            })
            .then(function (res) {
                return res;
            })
            .catch(function (err) {
                return err.response;
            });

    }

    async patch(req) {
        const kcAdmin = await KeycloakHelper.getAdminToken();

        return await axios
            .patch(`${this.baseUrl}${req.url}`, req.body, {
                headers: {
                    "Authorization": `Bearer ${kcAdmin.access_token}`
                }
            })
            .then(function (res) {
                return res;
            })
            .catch(function (err) {
                return err.response;
            });

    }

    async delete(req) {
        const kcAdmin = await KeycloakHelper.getAdminToken();

        return await axios
            .delete(`${this.baseUrl}${req.url}`, {
                headers: {
                    "Authorization": `Bearer ${kcAdmin.access_token}`
                },
                data: req.body
            })
            .then(function (res) {
                return res;
            })
            .catch(function (err) {
                return err.response;
            });

    }
}

module.exports = KeycloakService;
