const axios = require("axios");

class CommonService {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
  }

  async get(req) {
    
    return await axios
      .get(`${this.baseUrl}${req.url}`)
      .then(function (res) {
        return res;
      })
      .catch(function (err) {
        return err.response;
      });

  }

  async post(req) {

    return await axios
      .post(`${this.baseUrl}${req.url}`, req.body)
      .then(function (res) {
        return res;
      })
      .catch(function (err) {
        return err.response;
      });
      
  }

  async put(req) {

    return await axios
      .put(`${this.baseUrl}${req.url}`, req.body, { headers: req.headers })
      .then(function (res) {
        return res;
      })
      .catch(function (err) {
        return err.response;
      });

  }

  async patch(req) {

    return await axios
      .patch(`${this.baseUrl}${req.url}`, req.body, { headers: req.headers })
      .then(function (res) {
        return res;
      })
      .catch(function (err) {
        return err.response;
      });

  }

  async delete(req) {
    return await axios
    .delete(`${this.baseUrl}${req.url}`, { headers: req.headers, data: req.body })
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      return err.response;
    });
  }
}

module.exports = CommonService;
