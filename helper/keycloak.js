const fetch = require("node-fetch");

const kcVariables = {
    'username': 'rhino-admin',
    'password': 'P@ssw0rd',
    'grant_type': 'password',
    'client_id': 'admin-cli'
};

class KeycloakHelper {
    static async getAdminToken() {

        let variables = kcVariables;
        let formBody = [];
        for (let property in variables) {
            let encodedKey = encodeURIComponent(property);
            let encodedValue = encodeURIComponent(variables[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");
        const getTokenResponse = await fetch(`${process.env.KC_SERVER_URL}/auth/realms/visiondg/protocol/openid-connect/token`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json',
                'Cache-Control': 'no-cache'
            },
            body: formBody
        })

        
        return await getTokenResponse.json();
    }
}

module.exports = KeycloakHelper;
