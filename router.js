var indexRouter = require('./routes/index');
var catchRouter = require('./routes/catch');
var catchFileRouter = require('./routes/catch_file');
var secureRouter = require('./routes/secure');

module.exports = (app) => {
    app.use('/emu', indexRouter);

    app.use('/emu/catch', catchRouter);
    app.use('/emu/catch_file', catchFileRouter);
    app.use('/emu/secure', secureRouter);
};