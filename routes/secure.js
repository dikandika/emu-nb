const express = require('express');
const serviceHost = require('../helper/service_host');
const router = express.Router();
const CommonService = require('../services/common_service');
const kafka = require('kafka-node');
const { Producer } = require('kafka-node');
const log = require('../log');
const moment = require('moment');
const KeycloakService = require('../services/keycloak_service');
const CipherHelper = require('../helper/cipher');

const unknownService = "unknown";
const unknownServiceMessage = "Unknown Service Destination";

// KAFKA INIT
const client = new kafka.KafkaClient({ kafkaHost: process.env.KAFKA_HOST });
const producer = new Producer(client);

// KAFKA CLIENT LISTENER
client.on('ready', function () {
    log.info({
        info: 'Kafka Client Ready',
        timestamp: moment().format("YYYY/MM/DD HH:mm:ss"),
        at: 'routes/secure.js'
    });
});

client.on('error', function (err) {
    log.error({
        error: 'Kafka Client Error: ' + err,
        timestamp: moment().format("YYYY/MM/DD HH:mm:ss"),
        at: 'routes/secure.js'
    });
});

// KAFKA PRODUCER LISTENER
producer.on('ready', function () {
    log.info({
        info: 'Kafka Producer Ready',
        timestamp: moment().format("YYYY/MM/DD HH:mm:ss"),
        at: 'routes/secure.js'
    });
});

// KAFKA PRODUCE LOG
async function sendKafkaBOLog(request, result) {
    const requestedService = serviceHost(request.url);

    const payloads = [];

    const errorCode = [500, 501, 502, 503, 504, 400, 401, 402, 403, 404, 405];

    const status = !errorCode.includes(result?.status ?? 500) ? "success" : "failed"

    let payloadMessage = {
        url: `${requestedService.host}${request.url}`,
        body: (request.body ?? {}),
        timestamp: moment().format("YYYY/MM/DD HH:mm:ss"),
        status: status,
        user: {
            id: (request.headers["bo-user-id"] ?? "-"),
            name: (request.headers["bo-user-name"] ?? "-")
        },
        description: {
            service: requestedService,
            method: request.method,
            action: (request.headers["bo-user-action"] ?? "-")
        }
    };

    payloads.push({
        topic: process.env.KAFKA_TOPIC_BACKOFFICE_TRAIL,
        messages: JSON.stringify(payloadMessage),
        partition: 0
    });

    // PRODUCE TO KAFKA
    producer.send(payloads, function (err, data) {
        if (err) {
            log.error({
                host: process.env.KAFKA_HOST,
                payloads: payloads,
                info: err,
                event: 'kafka producer send',
                timestamp: new Date().toLocaleString(),
                at: 'routes/secure.js',
            });
        }

        log.info({
            host: process.env.KAFKA_HOST,
            payloads: payloads,
            info: "Success",
            event: 'kafka producer send',
            timestamp: new Date().toLocaleString(),
            at: 'routes/secure.js',
        });
    });
}

// LOCAL LOG
function createLog(request, result) {
    const requestedService = serviceHost(request.url);

    log.info({
        event: "request",
        url: `${requestedService.host}${request.url}`,
        method: request.method,
        headers: request.headers,
        body: (request.body ?? {}),
        status: result?.status ?? "-",
        service: requestedService,
        timestamp: moment().format("YYYY/MM/DD HH:mm:ss"),
        result: JSON.stringify(result?.data ?? {})
    });
}

router.get('/*', async function (req, res, next) {

    try {
        const decryptedRequest = CipherHelper.decryptRequest(req);

        req.url = decryptedRequest.url;
        if (decryptedRequest.body) {
            req.body = decryptedRequest.body
        }
        
        let requestUrl = req.url;

        console.log("requestUrl", requestUrl)

        const requestedService = serviceHost(requestUrl);

        let service = new CommonService(requestedService.host);

        if (requestedService.name === unknownService) {
            res.send({ message: unknownServiceMessage, data: [] });
        } else if (requestedService.name === "keycloak") {
            service = new KeycloakService(requestedService.host);
        }

        const response = await service.get(req);

        const result = CipherHelper.encryptResponse(response?.data ?? {});

        res.status((response?.status ?? 500)).send((result ?? {}));
    } catch (err) {
        res.status(500).send({
            "message": "Internal Server Error",
            "error": JSON.stringify(err)
        });
    }
});

router.post('/*', async function (req, res, next) {
    try {
        const decryptedRequest = CipherHelper.decryptRequest(req);

        req.url = decryptedRequest.url;
        if (decryptedRequest.body) {
            req.body = decryptedRequest.body
        }

        const requestUrl = req.url;

        const requestedService = serviceHost(requestUrl);

        let service = new CommonService(requestedService.host);

        if (requestedService.name === unknownService) {
            res.send({ message: unknownServiceMessage, data: [] });
        } else if (requestedService.name === "keycloak") {
            service = new KeycloakService(requestedService.host);
        }

        const response = await service.post(req);

        createLog(req, response);

        await sendKafkaBOLog(req, response);

        if (requestedService.name === "keycloak" && (requestUrl.includes('groups') || requestUrl.includes('roles') || requestUrl.includes('users'))) {
            const result = CipherHelper.encryptResponse(response?.headers ?? {});

            res.status((response?.status ?? 500)).send(result ?? {});
        } else {
            const result = CipherHelper.encryptResponse(response?.data ?? {});

            res.status((response?.status ?? 500)).send(result ?? {});
        }

    } catch (err) {
        res.status(500).send({
            "message": "Internal Server Error",
            "error": JSON.stringify(err)
        });
    }
});

router.put('/*', async function (req, res, next) {

    try {
        const decryptedRequest = CipherHelper.decryptRequest(req);

        req.url = decryptedRequest.url;
        if (decryptedRequest.body) {
            req.body = decryptedRequest.body
        }

        const requestUrl = req.url;

        const requestedService = serviceHost(requestUrl);

        let service = new CommonService(requestedService.host);

        if (requestedService.name === unknownService) {
            res.send({ message: unknownServiceMessage, data: [] });
        } else if (requestedService.name === "keycloak") {
            service = new KeycloakService(requestedService.host);
        }

        const response = await service.put(req);

        createLog(req, response);

        await sendKafkaBOLog(req, response);

        const result = CipherHelper.encryptResponse(response?.data ?? {});

        res.status((response?.status ?? 500)).send(result ?? {});

    } catch (err) {
        res.status(500).send({
            "message": "Internal Server Error",
            "error": JSON.stringify(err)
        });
    }


});

router.patch('/*', async function (req, res, next) {

    try {
        const decryptedRequest = CipherHelper.decryptRequest(req);

        req.url = decryptedRequest.url;
        if (decryptedRequest.body) {
            req.body = decryptedRequest.body
        }

        const requestUrl = req.url;

        const requestedService = serviceHost(requestUrl);

        let service = new CommonService(requestedService.host);

        if (requestedService.name === unknownService) {
            res.send({ message: unknownServiceMessage, data: [] });
        } else if (requestedService.name === "keycloak") {
            service = new KeycloakService(requestedService.host);
        }

        const response = await service.patch(req);

        createLog(req, response);

        await sendKafkaBOLog(req, response);

        const result = CipherHelper.encryptResponse(response?.data ?? {});

        res.status((response?.status ?? 500)).send(result ?? {});
    } catch (err) {
        res.status(500).send({
            "message": "Internal Server Error",
            "error": JSON.stringify(err)
        });
    }


});

router.delete('/*', async function (req, res, next) {
    try {
        const decryptedRequest = CipherHelper.decryptRequest(req);

        req.url = decryptedRequest.url;
        if (decryptedRequest.body) {
            req.body = decryptedRequest.body
        }

        const requestUrl = req.url;

        const requestedService = serviceHost(requestUrl);

        let service = new CommonService(requestedService.host);

        if (requestedService.name === unknownService) {
            res.send({ message: unknownServiceMessage, data: [] });
        } else if (requestedService.name === "keycloak") {
            service = new KeycloakService(requestedService.host);
        }

        const response = await service.delete(req);

        createLog(req, response);

        await sendKafkaBOLog(req, response);

        const result = CipherHelper.encryptResponse(response?.data ?? {});

        res.status((response?.status ?? 500)).send(result ?? {});
    } catch (err) {
        res.status(500).send({
            "message": "Internal Server Error",
            "error": JSON.stringify(err)
        });
    }


});

module.exports = router;
