const crypto = require("crypto");
const path = require("path");
const fs = require("fs");
const CryptoJS = require("crypto-js");

class CipherHelper {
    static rsaEncrypt(message) {
        const absolutePath = path.resolve('certs\\public_key.pem')
        const publicKey = fs.readFileSync(absolutePath, 'utf8')
        const buffer = Buffer.from(message, 'utf8')
        // THIS ENCRYPT METHOD MATCH WITH "JSENCRYPT" ON CLIENT SIDE ENCRYPTING
        const encrypted = crypto.publicEncrypt({
            key: publicKey.toString(),
            passphrase: '',
            padding: crypto.constants.RSA_PKCS1_PADDING
        }, buffer)
        return encrypted.toString('base64')
    }

    static rsaDecrypt(message) {
        const absolutePath = path.resolve('certs\\private_key.pem')
        const privateKey = fs.readFileSync(absolutePath, 'utf8')
        const buffer = Buffer.from(message, 'base64')
        // THIS DECRYPT METHOD MATCH WITH "JSENCRYPT" ON CLIENT SIDE DECRYPTING
        const decrypted = crypto.privateDecrypt(
            {
                key: privateKey.toString(),
                passphrase: '',
                padding: crypto.constants.RSA_PKCS1_PADDING
            },
            buffer,
        )
        return decrypted.toString('utf8')
    }

    static aesEncrypt(message, key) {
        const encrypted = CryptoJS.AES.encrypt(message, key);
        return encrypted.toString();
    }

    static aesDecrypt(message, key) {
        const decrypted = CryptoJS.AES.decrypt(message, key);
        return decrypted.toString(CryptoJS.enc.Utf8);
    }

    static decryptRequest(req) {
        const query = req.query;

        let decryptedParams = "";
        if (query.param) {
            const splittedParam = query.param.split('-');
            // BECAUSE PLUS SYMBOL(+) ALWAYS DISAPPEARED, REPLACE WHITESPACE INTO PLUS SYMBOL
            const reqPlainKey = (splittedParam[0] ?? "").replace(/ /g, "+");
            const reqParams = (splittedParam[1] ?? "").replace(/ /g, "+");

            const decryptedPlainKey = this.rsaDecrypt(reqPlainKey);
            decryptedParams = this.aesDecrypt(reqParams, decryptedPlainKey);
        }

        let decryptedBody = {};
        if (req.body && req.body.data) {
            const splittedBody = req.body.data.split('-');
            // BECAUSE PLUS SYMBOL(+) ALWAYS DISAPPEARED, REPLACE WHITESPACE INTO PLUS SYMBOL
            const reqPlainKey = (splittedBody[0] ?? "").replace(/ /g, "+");
            const reqBody = (splittedBody[1] ?? "").replace(/ /g, "+");

            const decryptedPlainKey = this.rsaDecrypt(reqPlainKey);
            const decryptedReqBody = this.aesDecrypt(reqBody, decryptedPlainKey);

            if (decryptedReqBody) {
                decryptedBody = JSON.parse(decryptedReqBody);
            }
        }

        let finalUrl = req.path;

        if (decryptedParams) {
            finalUrl += `?${decryptedParams}`;
        }

        return {
            url: finalUrl,
            body: decryptedBody
        };
    }

    static encryptResponse(res) {
        // CREATE 12 CHAR RANDOM NUMBER AND LETTER
        const plainKey = crypto.randomBytes(6).toString('hex');

        const encryptedPlainKey = this.rsaEncrypt(plainKey);

        const encryptedResponse = this.aesEncrypt(JSON.stringify(res), plainKey);

        return {
            "data": `${encryptedPlainKey}-${encryptedResponse}`
        };
    }
}



module.exports = CipherHelper;
