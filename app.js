var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

require('dotenv').config();

const tokenValidation = require('./middleware/token_validation');
const router = require('./router');

var app = express();

app.use(logger('dev'));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// KEYCLOAK TOKEN VALIDATION
// app.use(tokenValidation);

router(app);

module.exports = app;
