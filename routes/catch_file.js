var express = require("express");
const serviceHost = require("../helper/service_host");
var router = express.Router();
const http = require("http");
const https = require("https");
var fs = require("fs");
var FormData = require("form-data");
const CommonHelper = require("../helper/common");
const fetch = require("node-fetch");
const kafka = require('kafka-node');
const { Producer } = require('kafka-node');
var multer = require("multer");
const appRoot = require("app-root-path");
const log = require('../log');
const moment = require('moment');

var storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, appRoot + "/uploads");
  },
  filename: (req, file, callback) => {
    callback(null, file.originalname);
  },
});

var upload = multer({
  storage: storage,
}).single("file");

const unknownService = "unknown";
const unknownServiceMessage = "Unknown Service Destination, File Storage Service is ...";

// KAFKA INIT
const client = new kafka.KafkaClient({ kafkaHost: process.env.KAFKA_HOST });
const producer = new Producer(client);

// KAFKA CLIENT LISTENER
client.on('ready', function () {
  log.info({
    info: 'Kafka Client Ready',
    timestamp: moment().format("YYYY/MM/DD HH:mm:ss"),
    at: 'routes/catch_file.js'
  });
});

client.on('error', function (err) {
  log.error({
    error: 'Kafka Client Error: ' + err,
    timestamp: moment().format("YYYY/MM/DD HH:mm:ss"),
    at: 'routes/catch_file.js'
  });
});

// KAFKA PRODUCER LISTENER
producer.on('ready', function () {
  log.info({
    info: 'Kafka Producer Ready',
    timestamp: moment().format("YYYY/MM/DD HH:mm:ss"),
    at: 'routes/catch_file.js'
  });
});

// KAFKA PRODUCE LOG
async function sendKafkaBOLog(request, result) {
  const requestedService = serviceHost(request.url);

  const payloads = [];

  let payloadMessage = {
    url: `${requestedService.host}${request.url}`,
    body: (request.body ?? {}),
    timestamp: moment().format("YYYY/MM/DD HH:mm:ss"),
    status: ((result?.status ?? "failed") === 500 ? "success" : "failed"),
    user: {
      id: (request.headers["bo-user-id"] ?? "-"),
      name: (request.headers["bo-user-name"] ?? "-")
    },
    description: {
      service: requestedService,
      method: request.method,
      response: result.data,
      action: (request.headers["bo-user-action"] ?? "-")
    }
  };

  payloads.push({
    topic: process.env.KAFKA_TOPIC_BACKOFFICE_TRAIL,
    messages: JSON.stringify(payloadMessage),
    partition: 0
  });

  // PRODUCE TO KAFKA
  producer.send(payloads, function (err, data) {
    if (err) {
      log.error({
        host: process.env.KAFKA_HOST,
        payloads: payloads,
        info: err,
        event: 'kafka producer send',
        timestamp: new Date().toLocaleString(),
        at: 'routes/catch_file.js',
      });
    }

    log.info({
      host: process.env.KAFKA_HOST,
      payloads: payloads,
      info: "Success",
      event: 'kafka producer send',
      timestamp: new Date().toLocaleString(),
      at: 'routes/catch_file.js',
    });
  });
}

router.get("/*", async function (req, res) {
  const requestUrl = req.url;

  const requestedService = serviceHost(requestUrl);

  if (requestedService.name === unknownService) {
    res.send({ message: unknownServiceMessage, data: ["huskar"] });
  }

  // await sendKafkaBOLog(req, { status: 200 });

  if (requestedService.host.includes('https')) {
    https.get(`${requestedService.host}${req.url}`, (stream) => {
      stream.pipe(res);
    });
  } else {
    http.get(`${requestedService.host}${req.url}`, (stream) => {
      stream.pipe(res);
    });
  }
});

router.post("/*", upload, async (req, res) => {
  const requestUrl = req.url;

  const requestedService = serviceHost(requestUrl);

  if (requestedService.name === unknownService) {
    res.send({ message: unknownServiceMessage, data: ["huskar"] });
  }

  try {
    const selectedFile = fs.createReadStream(req.file.path);

    let formData = new FormData();
    formData.append("file", selectedFile);

    if (req.body && Object.keys(req.body).length > 0) {
      for (const k in req.body) {
        formData.append(k, req.body[k]);
      }
    }

    const response = await fetch(`${requestedService.host}${requestUrl}`, {
      method: 'POST',
      body: formData
    }).catch(err => {
      console.error(err);
    });

    const result = await response.json();

    await sendKafkaBOLog({ 
      url: req.url, 
      headers: req.headers, 
      method: req.method, 
      body: { file: req.file.filename } 
    }, {
      status : response.status,
      data: result
    });

    res.send(result);
  } catch (err) {
    res.send({ message: "Error Uploading File", error: err.message });
  }
});

module.exports = router;
